// Add contains function to array object to check if element is in array
Array.prototype.contains = function(element){
  return this.indexOf(element) > -1;
};

function getActiveTab(callback) {
  browser.tabs.query({active: true, currentWindow: true}).then(
    (tabs) => { callback(tabs[0]) }
  );
}

getActiveTab(updatePageAction);

browser.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
  if (!changeInfo.url) {
    return;
  }
  getActiveTab((activeTab) => {
    if (tabId == activeTab.id) {
      updatePageAction(activeTab);
    }
  });
});

/*
Check for a match in the currently active tab, whenever a new tab becomes
active.
*/
browser.tabs.onActivated.addListener((activeInfo) => {
  getActiveTab(updatePageAction);
});

var platforms = [
  {
    liberapay_slug: 'bitbucket',
    regex: new RegExp('^https://bitbucket.org/([^/?#]+)'),
    slug_blacklist: ['account', 'legal', 'product', 'socialauth'],
  },
  {
    liberapay_slug: 'github',
    regex: new RegExp('^https://github.com/([^/?#]+)'),
    slug_blacklist: ['about', 'blog', 'business', 'contact', 'explore', 'features', 'join', 'login', 'marketplace', 'open-source', 'personal', 'pricing', 'site'],
  },
  {
    liberapay_slug: 'twitter',
    regex: new RegExp('^https://twitter.com/([^/?#]+)'),
    slug_blacklist: ['about', 'account', 'accounts', 'activity', 'all', 'announcements', 'anywhere', 'api_rules', 'api_terms', 'apirules', 'apps', 'auth', 'badges', 'blog', 'business', 'buttons', 'contacts', 'devices', 'direct_messages', 'download', 'downloads', 'edit_announcements', 'faq', 'favorites', 'find_sources', 'find_users', 'followers', 'following', 'friend_request', 'friendrequest', 'friends', 'goodies', 'help', 'home', 'i', 'im_account', 'inbox', 'invitations', 'invite', 'jobs', 'list', 'login', 'logo', 'logout', 'me', 'mentions', 'messages', 'mockview', 'newtwitter', 'notifications', 'nudge', 'oauth', 'phoenix_search', 'positions', 'privacy', 'public_timeline', 'related_tweets', 'replies', 'retweeted_of_mine', 'retweets', 'retweets_by_others', 'rules', 'saved_searches', 'search', 'sent', 'sessions', 'settings', 'share', 'signup', 'signin', 'similar_to', 'statistics', 'terms', 'tos', 'translate', 'trends', 'tweetbutton', 'twttr', 'update_discoverability', 'users', 'welcome', 'who_to_follow', 'widgets', 'zendesk_auth', 'media_signup'],
  },
];

function getLiberapayUrl(tab) {
  return platforms.reduce((found, platform) => {
    if (found !== null) {
      return found;
    } else {
      var match = platform.regex.exec(tab.url);
      if (match !== null) {
        var slug = match[1];
        if (!platform.slug_blacklist.contains(slug)) {
          return 'https://liberapay.com/on/' + platform.liberapay_slug + '/' + slug;
        }
      }
      return null;
    }
  }, null);
}

function updatePageAction(activeTab) {
  var found = getLiberapayUrl(activeTab) !== null;
  if (found) {
    browser.pageAction.show(activeTab.id);
  } else {
    browser.pageAction.hide(activeTab.id);
  }
}

browser.pageAction.onClicked.addListener(() => {
  getActiveTab((activeTab) => {
    if (url = getLiberapayUrl(activeTab)) {
      browser.tabs.update({url: url});
    }
  });
});
