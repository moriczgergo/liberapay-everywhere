Liberapay-Everywhere
===================

The yet unofficial Liberapay browser extension. We enable Liberapay
giving icons on many of your favorite sites:

- Bitbucket
- GitHub
- Twitter

This project was inspired by the now defunct
[Gratipay-everywhere](https://github.com/gratipay/gratipay-everywhere).

## Deployment

### Firefox

Execute `zip -r liberapay-everywhere.zip *` and upload it.

## License

[CC0 Public Domain Dedication](http://creativecommons.org/publicdomain/zero/1.0/),
included in `LICENSE` file.

[MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/), included in `LICENSE-MPL2` for `webextension-polyfill.js`.